import axiosOrders from "../axios-orders";

export const INPUT_CHANGE = "INPUT_CHANGE";

export const CLOSE_MODAL = "CLOSE_MODAL";

export const FETCH_TODOLIST_REQUEST = "FETCH_TODOLIST_REQUEST";
export const FETCH_TODOLIST_SUCCESS = "FETCH_TODOLIST_SUCCESS";
export const FETCH_TODOLIST_FAILURE = "FETCH_TODOLIST_FAILURE";

export const ADD_TASK_REQUEST = "ADD_TASK_REQUEST";
export const ADD_TASK_SUCCESS = "ADD_TASK_SUCCESS";
export const ADD_TASK_FAILURE = "ADD_TASK_FAILURE";

export const SWITCH_TASK_STATUS_REQUEST = "SWITCH_TASK_STATUS_REQUEST";
export const SWITCH_TASK_STATUS_SUCCESS = "SWITCH_TASK_STATUS_SUCCESS";
export const SWITCH_TASK_STATUS_FAILURE = "SWITCH_TASK_STATUS_FAILURE";

export const REMOVE_TASK_REQUEST = "REMOVE_TASK_REQUEST";
export const REMOVE_TASK_SUCCESS = "REMOVE_TASK_SUCCESS";
export const REMOVE_TASK_FAILURE = "REMOVE_TASK_FAILURE";

export const inputChange = value => ({type: INPUT_CHANGE, value});

export const closeModal = () => ({type: CLOSE_MODAL});

export const fetchToDoListRequest = () => ({type: FETCH_TODOLIST_REQUEST});
export const fetchToDoListSuccess = toDoList => ({type: FETCH_TODOLIST_SUCCESS, toDoList});
export const fetchToDoListFailure = () => ({type: FETCH_TODOLIST_FAILURE});

export const fetchToDoList = () => {
  return async dispatch => {
    dispatch(fetchToDoListRequest());

    try {
      const response = await axiosOrders.get("toDoList.json");
      dispatch(fetchToDoListSuccess(response.data));
    } catch (e) {
      dispatch(fetchToDoListFailure());
    }
  };
};

export const addTaskRequest = () => ({type: ADD_TASK_REQUEST});
export const addTaskSuccess = () => ({type: ADD_TASK_SUCCESS});
export const addTaskFailure = errorMsg => ({type: ADD_TASK_FAILURE, errorMsg});

export const addTask = newTask => {
  return async dispatch => {
    dispatch(addTaskRequest());

    try {
      await axiosOrders.post("toDoList.json", newTask);
      dispatch(addTaskSuccess());
      dispatch(fetchToDoList());
    } catch (e) {
      dispatch(addTaskFailure(e.message));
    }
  };
};

export const switchTaskStatusRequest = () => ({type: SWITCH_TASK_STATUS_REQUEST});
export const switchTaskStatusSuccess = () => ({type: SWITCH_TASK_STATUS_SUCCESS});
export const switchTaskStatusFailure = errorMsg => ({type: SWITCH_TASK_STATUS_FAILURE, errorMsg});

export const switchTaskStatus = (id, status) => {
  return async dispatch => {
    dispatch(switchTaskStatusRequest());

    try {
      await axiosOrders.patch(`toDoList/${id}.json`, {isActive: !status});
      dispatch(switchTaskStatusSuccess());
      dispatch(fetchToDoList());
    } catch (e) {
      dispatch(switchTaskStatusFailure(e.message));
    }
  }
};

export const removeTaskRequest = () => ({type: REMOVE_TASK_REQUEST});
export const removeTaskSuccess = () => ({type: REMOVE_TASK_SUCCESS});
export const removeTaskFailure = errorMsg => ({type: REMOVE_TASK_FAILURE, errorMsg});

export const removeTask = id => {
  return async dispatch => {
    dispatch(removeTaskRequest());

    try {
      await axiosOrders.delete(`toDoList/${id}.json`);
      dispatch(removeTaskSuccess());
      dispatch(fetchToDoList());
    } catch (e) {
      dispatch(removeTaskFailure(e.message));
    }
  }
};