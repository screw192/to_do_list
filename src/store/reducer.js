import {
  INPUT_CHANGE,
  CLOSE_MODAL,
  ADD_TASK_REQUEST,
  ADD_TASK_SUCCESS,
  ADD_TASK_FAILURE,
  FETCH_TODOLIST_REQUEST,
  FETCH_TODOLIST_SUCCESS,
  FETCH_TODOLIST_FAILURE,
  SWITCH_TASK_STATUS_REQUEST,
  SWITCH_TASK_STATUS_SUCCESS,
  SWITCH_TASK_STATUS_FAILURE,
  REMOVE_TASK_REQUEST,
  REMOVE_TASK_SUCCESS,
  REMOVE_TASK_FAILURE
} from "./actions";

const initialState = {
  toDoList: null,
  inputField: "",
  loading: true,
  error: false,
  errorMsg: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {...state, inputField: action.value};

    case CLOSE_MODAL:
      return {...state, error: false, errorMsg: ""};

    case FETCH_TODOLIST_REQUEST:
      return {...state, loading: true};
    case FETCH_TODOLIST_SUCCESS:
      return {...state, loading: false, toDoList: action.toDoList};
    case FETCH_TODOLIST_FAILURE:
      return {...state, loading: false};

    case ADD_TASK_REQUEST:
      return {...state, loading: true};
    case ADD_TASK_SUCCESS:
      return {...state, inputField: ""};
    case ADD_TASK_FAILURE:
      return {...state, loading: false, error: true, errorMsg: action.errorMsg};

    case SWITCH_TASK_STATUS_REQUEST:
      return {...state, loading: true};
    case SWITCH_TASK_STATUS_SUCCESS:
      return state;
    case SWITCH_TASK_STATUS_FAILURE:
      return {...state, loading: false, error: true, errorMsg: action.errorMsg};

    case REMOVE_TASK_REQUEST:
      return {...state, loading: true};
    case REMOVE_TASK_SUCCESS:
      return state;
    case REMOVE_TASK_FAILURE:
      return {...state, loading: false, error: true, errorMsg: action.errorMsg};

    default:
      return state;
  }
};

export default reducer;