import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchToDoList, removeTask, switchTaskStatus} from "../../store/actions";
import AddTaskForm from "../../components/AddTaskForm/AddTaskForm";
import Task from "../../components/Task/Task";
import "./ToDoList.css";

const ToDoList = () => {
  const dispatch = useDispatch();
  const toDoList = useSelector(state => state.toDoList);

  useEffect(() => {
    dispatch(fetchToDoList());
  }, [dispatch]);

  let taskList = null;
  if (toDoList !== null) {
    taskList = (
        <>
          {Object.keys(toDoList).map(id => {
            return <Task
                key={id}
                taskText={toDoList[id].task}
                taskStatus={toDoList[id].isActive}
                switchStatus={() => dispatch(switchTaskStatus(id, toDoList[id].isActive))}
                remove={() => dispatch(removeTask(id))}
            />
          })}
        </>
    );
  }

  return (
      <div className="ToDoList">
        <AddTaskForm />
        {taskList}
      </div>
  );
};

export default ToDoList;