import React from "react";
import {useDispatch, useSelector} from "react-redux";

import {inputChange, addTask} from "../../store/actions";
import "./AddTaskForm.css";

const AddTaskForm = () => {
  const dispatch = useDispatch();
  const inputField = useSelector(state => state.inputField);

  const inputFieldChange = event => dispatch(inputChange(event.target.value));
  const addNewTask = () => dispatch(addTask({
    isActive: true,
    task: inputField
  }));

  return (
      <div className="AddTaskForm">
        <input
            className="TaskFormInput"
            type="text"
            value={inputField}
            onChange={inputFieldChange}
        />
        <button
            className="TaskFormButton"
            disabled={!inputField}
            onClick={addNewTask}
        >
          Add
        </button>
      </div>
  );
};

export default AddTaskForm;