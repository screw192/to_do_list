import React from 'react';
import {useDispatch} from "react-redux";

import {closeModal} from "../../../store/actions";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";

const Modal = ({title, message}) => {
  const dispatch = useDispatch();

  const closeHandler = () => dispatch(closeModal());

  return (
      <Backdrop clickHandler={closeHandler}>
        <div className="Modal">
          <div className="ModalTop">
            <h4 className="ModalTitle">{title}</h4>
            <button
                className="ModalCloseButton"
                onClick={closeHandler}
            >&#10005;</button>
          </div>
          <p className="ModalMessage">{message}</p>
        </div>
      </Backdrop>
  );
};

export default Modal;