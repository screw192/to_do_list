import React from "react";

import "./Task.css";

const Task = props => {
  let taskClass = ["Task"];
  let checkBox;

  if (props.taskStatus) {
    checkBox = "\u2610";
  } else if (!props.taskStatus) {
    taskClass.push("TaskFinished");
    checkBox = "\u2611";
  }

  return (
      <div className="TaskBody">
        <div className={taskClass.join(" ")}>
          <span className="CheckBox" onClick={props.switchStatus}>{checkBox}</span>
          <p>{props.taskText}</p>
        </div>
        <button className="RemoveButton" onClick={props.remove}>&#10005;</button>
      </div>
  )
};

export default Task;