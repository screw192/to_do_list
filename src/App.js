import React from "react";
import {useSelector} from "react-redux";

import Preloader from "./components/UI/Preloader/Preloader";
import ToDoList from "./containers/ToDoList/ToDoList";
import Modal from "./components/UI/Modal/Modal";

const App = () => {
  const isLoading = useSelector(state => state.loading);
  const isError = useSelector(state => state.error);
  const errorMessage = useSelector(state => state.errorMsg);

  let preloader;
  isLoading ? preloader = <Preloader/> : preloader = null;

  let error;
  isError ? error = <Modal title={"Oops..."} message={errorMessage}/> : error = null;

  return (
      <>
        {error}
        {preloader}
        <ToDoList />
      </>
  );
};

export default App;
