import axios from "axios";

const axiosOrders = axios.create({
  baseURL: "https://screw192-js9-todolist-default-rtdb.firebaseio.com/"
});

export default axiosOrders;